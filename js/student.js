let validStudentIdRegex = /^s[0-9]{7}/;
class Student extends Person{
    constructor(firstName,lastName,studentId){
        super(firstName,lastName);
        this.studentId = studentId;
    }
    get studentId(){
        return this._studentId;
    }

    set studentId(studentId){
        this._studentId = validStudentIdRegex.test(studentId)?studentId:"INVALID STUDENT ID";
    }
    study(){
        consolePrint(""+this.firstName+" "+this.lastName+" is studying...");
    }

    toString(){
        return `Student[firstname=${this.firstName},lastname=${this.lastName},id=${this.studentId}]`;
    }
}