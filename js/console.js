let c;

function initConsole(){
    c = document.getElementById("console")
}

function consolePrint(text){
    text += "<br/>";
    c.innerHTML += text;
}

function consoleClear(){
    //let c = document.getElementById("console");
    c.innerHTML = "";
}

function getConsole(){
    return c;
}

window.addEventListener("load", initConsole);