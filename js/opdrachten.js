function consolePrintTitle(text) {
    //consolePrint("");
    consolePrint(text);
    consolePrint("===========");
}

function h2opdr18() {
    consolePrintTitle("H2 opdracht 18:")
    function getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
      }
    
      let lottonumbers = []; //lege array waarin de lottonummers komen te staan
      do{
          let nb = getRandomIntInclusive(1,42); //genereer een getal tussen 1 en 42 (inclusief)
          //check if nb is already in list
          let isDouble = false;
          for(let el of lottonumbers){
            if(nb==el){
                isDouble = true;
                break;
            }
          }
          //indien niet al aanwezig in de lijst met lottonummers, voegen we het getal toe aan de lijst.
          if(!isDouble){
              lottonumbers.push(nb);
          }
      }while(lottonumbers.length<6);
      //sorteer van klein naar groot
      lottonumbers.sort((a,b)=>a-b);
      consolePrint("De lotto getallen van klein naar groot gesorteerd zijn:");
      for(let el of lottonumbers){
          consolePrint(el);
      }
}

function round(number,precision){
    let factor = 10**precision;
    return Math.round(number*factor)/factor;
}

function h2opdr23() {
    consolePrintTitle("H2 opdracht 23:")
    
    consolePrint("***create a shape with position (2,4):***");
    shape = new Shape(2,4);
    consolePrint(shape.toString());
    consolePrint("***move this shape to position (0,0):***");
    shape.move(0,0);
    consolePrint(shape.toString());
    
    consolePrint("***create rectangle with position (2,0) and width=6 and height=4:***");
    rect = new Rectangle(2,0,6,4);
    consolePrint(rect.toString());
    //***moving rectangle to (3,3) and scaling with factor 0.5***;
    consolePrint("***moving rectangle to (3,3) and scaling with factor 0.5***");
    rect.move(3,3);
    rect.scale(0.5);
    consolePrint(rect.toString());
    
    //***Create a circle with no argument constuctor (al is initialized to zero normally:***;
    consolePrint("***Create a circle with no argument constuctor (al is initialized to zero normally:***");
    circle = new Circle();
    consolePrint(circle.toString());
    //***try to set a negative radius. Everything should still be zero***
    consolePrint("***try to set a negative radius. Everything should still be zero***");
    circle.radius=-5;
    consolePrint(circle.toString());
    //***now we choose radius = 3:***
    consolePrint("***now we choose radius = 3:***");
    circle.radius=3;
    consolePrint(circle.toString());
    //***finally we scale the circle with a factor 2, and then move it to (2,1):***
    consolePrint("***finally we scale the circle with a factor 2, and then move it to (2,1):***");
    circle.scale(2);
    circle.move(2,1);
    consolePrint(circle.toString());
    
    consolePrint("***Create triangle with width=8,height=4,perpendicular=3 at pos(0,0):***");
    triangle = new Triangle(0,0,8,4,3);
    consolePrint(triangle.toString());
    consolePrint("***Now we scale with a factor 2 and move to (-5,3):***");
    triangle.scale(2);
    triangle.move(-5,3);
    consolePrint(triangle.toString());

    consolePrint("--------------------------DEEL2 PERSON HIERARCHY---------------------------------");
        p1 = new Person("Kris","Coolen");
    consolePrint(p1.toString());
    p2 = new Person("Ann-Sophie","Van der Stukken-Van Wanten");
    consolePrint(p2.toString());
    p3 = new Person("jan-pieter","Uit-de broek");
    consolePrint(p3.toString());
    p4 = new Person("3","   ");
    consolePrint(p4.toString());

    s1 = new Student("Kris","Coolen","s0174391");
    consolePrint(s1.toString());
    s1.study();
    s2 = new Student("Kris","Coolen","s017439");
    consolePrint(s2.toString());
    s3 = new Student("Kris","Coolen","0174391s");
    consolePrint(s3.toString());

    t1 = new Teacher("Kris","Coolen","u0063729");
    consolePrint(t1.toString());
    t1.teach();

    t2 = new Teacher("Kris","Coolen","s0174391");
    consolePrint(t2.toString());
    }

function h3opdr1() {
    consolePrintTitle("H3 opdracht 1:");
    consolePrint("Hello world!");
}

function h3opdr2() {
    consolePrintTitle("H3 opdracht 2:")
    consolePrint("screen x: " + window.screenX);
    consolePrint("screen y: " + window.screenY);
    consolePrint("screen width: " + screen.width);
    consolePrint("screen height: " + screen.height);
    consolePrint("screen availWidth: " + screen.availWidth);
    consolePrint("screen availHeight: " + screen.availHeight);
}

function h3opdr3() {
    consolePrintTitle("H3 opdracht 3:")
    let name = prompt("Enter your name: ");
    let answer = confirm("Is your name correct, " + name +"?");
    if(answer){
        alert("Hello " + name);
    }
    else{
        alert("No name");
    }

}

function h3opdr4() {
    consolePrintTitle("H3 opdracht 4:");
    consolePrint("Naam en versie van de browser: " + navigator.appName+navigator.appVersion);
    consolePrint("Naam besturingssysteem: " + navigator.platform);
    isOnLine = navigator.onLine?"Online":"Offline";
    consolePrint("Your browser is " +isOnLine);
    isCookieEnabled = navigator.cookieEnabled;
    if(isCookieEnabled){
        consolePrint("Cookies are enabled");
    }
    else  consolePrint("Cookies are disabled");
    if(!isCookieEnabled){
        location.replace("nocookies.html");
    }
}

function h3opdr6() {
    consolePrintTitle("H3 opdracht 6:");
    
    const today = new Date();
    if(today.getHours()>=6 && today.getHours()<12){
        consolePrint("Goede morgen");
    }
    else if(today.getHours()>=12 && today.getHours()<18){
        consolePrint("Goede Middag");
    }
    else{
        consolePrint("Goede avond");
    }
   consolePrint("<strong>"+today.toLocaleString()+"</strong>");
 
}

function h3opdr7() {
    consolePrintTitle("H3 opdracht 7:");
        
    const today = new Date();
    if(today.getHours()>=6 && today.getHours()<12){
        consolePrint("Goede morgen");
        getConsole().style["background-image"]="url(img/sun.png)"
    }
    else if(today.getHours()>=12 && today.getHours()<18){
        consolePrint("Goede Middag");
        getConsole().style["background-image"] = "url(img/sun.png)";
        }
    else{
        consolePrint("Goede avond");
        getConsole().style["background-image"] = "url(img/moon.jpg)";
    }
   consolePrint("<strong>"+today.toLocaleString()+"</strong>");
}

function h3opdr8() {
    consolePrintTitle("H3 opdracht 8:");
    const today = new Date();
    if(today.getHours()>=6 && today.getHours()<12){
        consolePrint("Goede morgen");
        let cssNode = getConsole();
        cssNode.parentNode.setAttribute("class","center day");
    }
    else if(today.getHours()>=12 && today.getHours()<18){
        consolePrint("Goede Middag");
        let cssNode = getConsole();
        cssNode.parentNode.setAttribute("class","center day");
        }
    else{
        consolePrint("Goede avond");
        let cssNode = getConsole();
        cssNode.parentNode.setAttribute("class","center night");
    }
   consolePrint("<strong>"+today.toLocaleString()+"</strong>");

}