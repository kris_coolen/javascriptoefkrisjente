let validNameRegex = /^([A-Za-z]+)([\-\s][A-Za-z])*/;
class Person{
    constructor(firstName,lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    get firstName(){
        return this._firstName;
    }
    get lastName(){
        return this._lastName;
    }

    set firstName(firstName){
        this._firstName = validNameRegex.test(firstName)?firstName:"INVALID NAME";
    }

    set lastName(lastName){
        this._lastName = validNameRegex.test(lastName)?lastName:"INVALID NAME";
    }

    toString(){
        return `Person[Firstname: ${this.firstName}, Lastname: ${this.lastName}]`
    }
}