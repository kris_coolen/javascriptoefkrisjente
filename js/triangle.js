class Triangle extends Shape{

    constructor(x=0,y=0,width=0,height=0,perpendicular=0){
        super(x,y);
        this.width=width;
        this.height=height;
        this.perpendicular=perpendicular;
    }

    get width(){
        return this._width;
    }

    set width(width){
        this._width = width<0?0:width; 
    }

    get height(){
        return this._height;
    }

    set height(height){
        this._height = height<0?0:height; 
    }

    get perpendicular(){
        return this._perpendicular;
    }

    set perpendicular(perpendicular){
        this._perpendicular = perpendicular<0?0:perpendicular;
    }

    get area(){
        return (this._width*this._height)/2;
    }
    get perimeter(){
        let s1 = Math.sqrt(this._perpendicular**2+this._height**2);
        let s2 = Math.sqrt((this._width-this._perpendicular)**2 + this._height**2);
        return this._width+s1+s2;
    }

    scale(factor){
        this.width *=factor;
        this.height*=factor;
        this.perpendicular*=factor;
    }

    toString(){
        return "Triangle<br/>-----------<br/>&nbsp;"+
        super.toString()+"<br/>&nbsp;width="+
        this.width+"<br/>&nbsp;height="+this.height+
        "<br/>&nbsp;perpendicular="+this.perpendicular+
        "<br/>&nbsp;area="+this.area+
        "<br/>&nbsp;perimeter="+round(this.perimeter,2)
	}


}