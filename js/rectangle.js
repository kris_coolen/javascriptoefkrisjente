class Rectangle extends Shape{
    
    constructor(x=0,y=0,width=0,height=0){
        super(x,y);
        this.width=width;
        this.height=height;
    }
    get width(){
        return this._width;
    }
    set width(width){
        this._width=width<0?0:width;
    }

    get height(){
        return this._height;
    }
    set height(height){
        this._height =height<0?0:height;
    }

    get area(){
        return this._height * this._width;
    }

    get perimeter(){
        return 2*(this._height + this._width);
    }

    scale(factor){
        this.width*=factor;
        this.height*=factor;
    }

    /*toString(){
        return `Rectangle---------
    ${super.toString()}
    width=${this.width}
    height=${this.height}
    area=${this.area}
    perimeter=${this.perimeter}`;
	}*/
	
	toString(){
		return "Rectangle<br/>-----------<br/>&nbsp;"+
		super.toString()+"<br/>&nbsp;width="+
		this.width+"<br/>&nbsp;height="+
		this.height+"<br/>&nbsp;area="+this.area+
		"<br/>&nbsp;perimeter="+this.perimeter
	}

}