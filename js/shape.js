class Shape{

    constructor(x=0,y=0){
        this.x=x;
        this.y=y;
    }
    get x(){
        return this._x;
    }
    set x(x){
        this._x =x;
    }
    get y(){
        return this._y;
    }
    set y(y){
        this._y=y;
    }
    move(x,y){
        this.x=x;
        this.y=y;
    }
    toString(){
        return `Shape[x=${this.x},y=${this.y}]`;
    }
}