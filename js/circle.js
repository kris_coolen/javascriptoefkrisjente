class Circle extends Shape{

    constructor(x=0,y=0,radius=0){
        super(x,y);
        this.radius=radius;
    }

    get radius(){
        return this._radius;
    }

    set radius(radius){
        this._radius=radius<0?0:radius;
    }

    get area(){
        return Math.PI*this._radius**2;
    }

    get perimeter(){
        return 2*Math.PI*this._radius;
    }

    scale(factor){
        this.radius *=factor;
    }

    /*toString(){
        return `Circle\n------
    ${super.toString()}
    radius=${this.radius}
    area=${round(this.area,2)}
    perimeter=${round(this.perimeter,2)}`;
    }*/
    
    toString(){
        return "Circle<br/>-----------<br/>&nbsp;"+
        super.toString()+"<br/>&nbsp;radius="+
        this.radius+"<br/>&nbsp;area="+round(this.area,2)+
        "<br/>&nbsp;perimeter="+round(this.perimeter,2);
    }
    
}