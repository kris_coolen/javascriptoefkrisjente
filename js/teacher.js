let validEmployeeIdRegex = /^u[0-9]{7}/;
class Teacher extends Person{
    constructor(firstName,lastName,employeeId){
        super(firstName,lastName);
        this.employeeId = employeeId;
    }
    get employeeId(){
        return this._employeeId;
    }

    set employeeId(employeeId){
        this._employeeId = validEmployeeIdRegex.test(employeeId)?employeeId:"INVALID EMPLOYEE ID";
    }
    teach(){
        consolePrint(""+this.firstName+" "+this.lastName+" is teaching...");
    }

    toString(){
        return `Teacher[firstname=${this.firstName},lastname=${this.lastName},id=${this.employeeId}]`;
    }
}